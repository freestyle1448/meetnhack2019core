package ru.omprussia.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "items")
public class Item {
    private Long barcode;
    private Double price;
    private Long countSold = 0L;
    private String photoUrl;
}
