package ru.omprussia.services;

import org.springframework.stereotype.Service;
import ru.omprussia.models.Item;
import ru.omprussia.repositories.ItemsRepository;
import ru.omprussia.repositories.ManualRepository;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class AdminServiceImpl implements AdminService {
    private final ItemsRepository itemsRepository;
    private final ManualRepository manualRepository;

    public AdminServiceImpl(ItemsRepository itemsRepository, ManualRepository manualRepository) {
        this.itemsRepository = itemsRepository;
        this.manualRepository = manualRepository;
    }

    @Override
    public CompletableFuture<List<Item>> getItems() {
        return CompletableFuture.completedFuture(itemsRepository.findAll());
    }

    @Override
    public CompletableFuture<Boolean> addItem(Item item) {
        return CompletableFuture.completedFuture(itemsRepository.insert(item) != null);
    }

    @Override
    public CompletableFuture<Boolean> updateItems(List<Item> items) {
        return CompletableFuture.completedFuture(items.parallelStream().allMatch(manualRepository::updateItem));
    }
}
