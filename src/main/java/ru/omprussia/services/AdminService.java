package ru.omprussia.services;

import org.springframework.scheduling.annotation.Async;
import ru.omprussia.models.Item;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Async
public interface AdminService {
    CompletableFuture<List<Item>> getItems();

    CompletableFuture<Boolean> addItem(Item item);

    CompletableFuture<Boolean> updateItems(List<Item> items);
}
