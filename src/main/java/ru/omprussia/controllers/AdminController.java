package ru.omprussia.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.omprussia.models.Item;
import ru.omprussia.services.AdminService;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
public class AdminController {
    private final Gson gson = new Gson();
    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("/getItems")
    public Callable<List<Item>> getItems() {
        return () -> adminService.getItems().get();
    }

    @PostMapping("/addItem")
    public Callable<Boolean> addItem(@ModelAttribute("item") Item item) {
        return () -> adminService.addItem(item).get();
    }

    @PostMapping("/updateItems")
    public Callable<Boolean> updateCount(String items) {
        List<Item> itemsList = gson.fromJson(items, new TypeToken<List<Item>>() {
        }.getType());
        return () -> adminService.updateItems(itemsList).get();
    }
}
