package ru.omprussia.repositories;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import ru.omprussia.models.Item;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Boolean updateItem(Item item) {
        Query findItem = new Query(Criteria
                .where("barcode").is(item.getBarcode()));
        Update update = new Update();
        update.inc("countSold", item.getCountSold());

        return mongoTemplate.findAndModify(findItem, update, Item.class) != null;
    }
}
