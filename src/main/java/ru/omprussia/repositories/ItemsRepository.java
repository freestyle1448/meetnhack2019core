package ru.omprussia.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import ru.omprussia.models.Item;

public interface ItemsRepository extends MongoRepository<Item, ObjectId> {
}
